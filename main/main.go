package main

import (
	"flag"
	"fmt"
	"time"

	"legitlab.letv.cn/wuxingyi/sheepdog/gateway"
	"legitlab.letv.cn/wuxingyi/sheepdog/membership"
)

const LeaseDuration = 5
const Copies = 3

var NodeId = flag.Uint("id", 1234, "unique id of this process")
var Vnodes = flag.Uint("vnodes", 16, "number of vnodes")
var Zone = flag.Uint("zone", 124, "the zone this node belongs to")
var Ip = flag.String("ip", "127.0.0.1", "the ip of this node")
var Port = flag.String("port", "9999", "the port of this node")

func main() {
	flag.Parse()
	go membership.StartMembership(*NodeId, *Vnodes, *Zone, LeaseDuration, *Ip, *Port)

	time.Sleep(15 * time.Second)
	tickch := time.Tick(5 * time.Second)
	for {
		select {
		case <-tickch:
			nodes, err := gateway.ObjectToNodes("hehe", Copies)
			if err != nil {
				fmt.Print(err)
			} else {
				for i := range nodes {
					fmt.Print(nodes[i].NodeId, " ")
				}
			}
			fmt.Println()
		}
	}
}
