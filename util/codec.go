package util

import (
	"bytes"
	"errors"

	"github.com/ugorji/go/codec"
)

func Encode(t MessageType, v interface{}) (*bytes.Buffer, error) {
	buf := bytes.NewBuffer(nil)
	buf.WriteByte(uint8(t))
	hd := codec.MsgpackHandle{}
	enc := codec.NewEncoder(buf, &hd)
	encodeerr := enc.Encode(v)
	return buf, encodeerr
}

func Decode(t MessageType, Value []byte, v interface{}) (err error) {
	r := bytes.NewBuffer(Value)
	readType, _ := r.ReadByte()
	if readType != uint8(t) {
		err = errors.New("this is not the type you want")
		return
	}

	hd := codec.MsgpackHandle{}
	dec := codec.NewDecoder(r, &hd)
	err = dec.Decode(v)
	return
}
