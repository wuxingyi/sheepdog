package util

type MessageType uint8

//a sheepdog node
type SHEEPDOGNODE struct {
	NodeId    uint //wuxingyi: node的id
	NR_vnodes uint //wuxingyi:vnode的个数
	Zone      uint //wuxingyi:节点所属的zone
	//Space     uint //wuxingyi:节点的磁盘空间
	Ip   string //used for consistent hashing
	Port string
}

func NewSheepdogNode(nodeid, vnodes, zone uint, ip string, port string) *SHEEPDOGNODE {
	return &SHEEPDOGNODE{NodeId: nodeid, NR_vnodes: vnodes, Zone: zone, Ip: ip, Port: port}
}
