# 设计和实现细节
## master选举  

master是全局唯一的，职责是存储集群的元数据，如其他节点列表、集群节点个数、集群master、集群的副本数等配置。 
节点启动时，为了避免一个新加入的节点成为master导致的cluster map同步，需要避免新加入节点成为master,新节点  
成为master的条件是join失败，此时如果集群中有其他节点，为了不给新加入的节点有机会成为master，我们让新加入  
节点休眠LeaseDuration+1秒之后，再次发起join请求，相当于主动退出master竞选，如果重新发送的join请求还是没人  
受理，那么可以认为集群中没有其他节点，从而新加入节点竞选并成为master。
实现上，每个节点都会通过ectd的watch API来监听/sheepdog/master这个key，而当前的master会通过不停的
刷新keepalive来维持其master角色。如果当前的master进程挂点，那么其lease失效，其他节点收到通知之后
立即发起master竞争操作，成功者成为master。

为了保证选择的正确性，在实现上使用了etcdv3的transaction API，第一个竞争上岗的节点成为master，那么
/sheepdog/master这个key的version就是0，新加入节点启动时尝试成为master，通过以下代码进行测试：
```
    req := clientv3.OpPut(MasterNode, buf.String(), clientv3.WithLease(leaseresp.ID))
    cond := clientv3.Compare(clientv3.Version(MasterNode), "=", 0)
    resp, err := c.Txn(context.TODO()).If(cond).Then(req).Commit()
```
只有之前没有人设置过/sheepdog/master这个key，或者持有者的lease已经过期导致key已失效的情况下，才能
成功。这样就保证了master的唯一性和选举的正确性。

## 节点如何加入集群
为了让待加入节点N能够知道集群元数据，待加入节点N需要首先将自己的元数据(NodeId, Vnodes, Zone)发到  
/sheepdog/member/nodeid这个key上，其他节点(包括master节点)会在这个/sheepdog/member这个prefix上设置watch，  
收到消息之后, 第一步，master将自己的元数据发送到/sheepdog/clustermap这个key上，而集群的其他节点不做处理。  
第二步，因为所有节点都会在/sheepdog/clustermap上设置watch，除了master之外，其他节点需要处理这个事件， 
处理上，就是更新为新的clustermap。
另外，每个节点必须维护/sheepdog/member/NodeId的keepalive，这样当节点挂掉时，其他节点能收到通知。

## member事件处理
所有节点都在/sheepdog/member这个prefix上设置watch, 当节点挂掉或加入时，每个member都能够修改集群元数据  

## 关于lease和keepalive
在etcd中，一组key可以依赖于同一个lease，可以通过同一套keepalive机制来维持活性，如果lease失效，那么这一组  
key全部失效。在当前的实现中，/sheepdog/member/nodeid和/sheepdog/master使用的是同一个lease，当一个master节点  
挂掉之后，master事件和member事件会同时发布给集群中的所有节点。

## join事件完整流程
在处理过程中，一个节点被集群成员接收，需要分为三个阶段： 
第一阶段节点N发消息到/sheepdog/member/nodeid上; 第二阶段master将节点加入集群，更新元数据，并发布到/sheepdog/clustermap  
第三阶段节点除master之外的所有节点更新为新版本的clustermap。

join流程中，要处理一个特殊的地方，就是节点启动时，刚好master死了，此时处在一个无政府的状态，没有人能够处理这个事件，此时会有一个  
goroutine在后台尝试查看节点是否已经被join到集群中，如果没有，再尝试几次发布join消息到/sheepdog/member/nodeid，如果一直失败，  
则认为集群中没有其他节点，从而这个节点直接成为master。

## leave事件处理
相比join事件， leave事件处理要复杂一下，这需要依赖于etcd的keepalive以及事件的有序性，通常情况下，节点L挂掉之后，master节点会  
将这个节点L除名并发布新版本的cluster map，其他各个节点收到clustermap之后做相应的更新操作。
假设master进程挂掉之后，在这期间又有其他节点挂掉，那么在极端情况下，可能会存在没有master处理节点leave事件的情况，这种情况可以通过
构造lease的方式复现，比如master的lease时间为100s，而其他所有节点的lease时间是10s，当master挂掉之后，最坏的情况下, 其他节点需要  
100s才能收到master挂掉的信息，而在这期间其他任何节点挂掉，都可能没有master进行处理，哪怕重新选出了一个master，也无法意识到有一个节点已经挂掉了。  
为了处理这种情况，在节点nodeL挂掉时，除了master节点之外的其他所有节点都会将节点L放到/sheepdog/pendingleaving/nodeL中，  
在正常情况下，master在收到自己的cluster map时，会扫一遍pendingleaving，如果有节点，并且已经不再cluster map中，  
那么就将/sheepdog/pendingleaving/nodeL删除.  
当经历了master挂掉之后的重新选举之后，新的master需要扫一遍pendingleaving，对照自己当前的map来确认是否在前任挂掉和自己上任期间有无  
节点leave，如果有，则在新版本的cluster map中，将这个节点除名，并删除对应的/sheepdog/pendingleaving/nodeL。