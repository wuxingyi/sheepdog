package gateway

import (
	"legitlab.letv.cn/wuxingyi/sheepdog/membership"
	"legitlab.letv.cn/wuxingyi/sheepdog/util"
)

func ObjectToNodes(key string, copies int) ([]*util.SHEEPDOGNODE, error) {
	hashring := membership.GetClusterHashRing()
	nodes, err := hashring.HashToNodes(key, copies)
	return nodes, err
}
