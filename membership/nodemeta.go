package membership

import (
	"context"
	"log"
	"os"
	"sync"
	"time"

	"github.com/coreos/etcd/clientv3"
	"legitlab.letv.cn/wuxingyi/sheepdog/consistenthash"
	"legitlab.letv.cn/wuxingyi/sheepdog/util"
)

var nodemeta *NodeMetaData

type NodeMetaData struct {
	Myself                                      *util.SHEEPDOGNODE
	IsJoined                                    bool
	IsMaster                                    bool
	NodeMap                                     map[uint]util.SHEEPDOGNODE //for search
	Ci                                          Clusterinfo                //for message
	JoinedLock, MasterLock, NodeMapLock, CILock sync.Mutex
	Client                                      *clientv3.Client
	Lease                                       clientv3.LeaseID
	LeaseDuration                               int64
	Wg                                          sync.WaitGroup
	HashRing                                    *consistenthash.ConsistentHashRing // every node should know the hash ring
}

// prepare etcd, including create client and a health check lease
func PrepareEtcdEnv(myself *NodeMetaData) error {
	c, err := GetClient([]string{"127.0.0.1:2379"}, time.Second, RetryCounter)

	if err != nil {
		log.Fatal(err)
	}

	//first check my existency
	getresp, err := c.Get(context.TODO(), MemberZone+string(myself.Myself.NodeId))
	if getresp.Count > 0 {
		log.Printf("node %d: lease has not expired, maybe there is another instance of mine, waiting for %d seconds and retry, now shooting myself", myself.Myself.NodeId, myself.LeaseDuration)
		os.Exit(1)
	}

	//ask for a lease
	leaseresp, err := c.Grant(context.TODO(), myself.LeaseDuration)
	if err != nil {
		log.Fatal(err)
	}

	//keep this session alive, if this daemon dead, the key will be expired.
	//MasterNode and MemberZone all relies on KeepAlive
	_, kaerr := c.KeepAlive(context.TODO(), leaseresp.ID)
	if kaerr != nil {
		log.Fatal(kaerr)
	}

	myself.Lease = leaseresp.ID
	myself.Client = c
	return nil
}

func initNodeMeta(nodeid, vnodes, zone uint, leaseDuration int64, ip string, port string) *NodeMetaData {
	//I know nothing about the cluster when booting
	Me := util.NewSheepdogNode(nodeid, vnodes, zone, ip, port)
	ci := Clusterinfo{Disable_recovery: false,
		Nr_nodes:  0,
		Epoch:     0,
		Nr_copies: 3,
		MasterID:  *util.NewSheepdogNode(0, 0, 0, "", ""),
	}
	nm := &NodeMetaData{Myself: Me,
		LeaseDuration: leaseDuration,
		IsMaster:      false,
		NodeMap:       make(map[uint]util.SHEEPDOGNODE),
		Ci:            ci,
		HashRing:      consistenthash.NewConsistentHashRing(),
	}

	//join test
	err := PrepareEtcdEnv(nm)
	if err != nil {
		log.Fatal(err)
	}
	return nm
}

func StartMembership(nodeid, vnodes, zone uint, leaseDuration int64, ip string, port string) {
	nm := initNodeMeta(nodeid, vnodes, zone, leaseDuration, ip, port)

	//global variable nodemeta is used to exported to other modules
	nodemeta = nm

	nm.Wg.Add(3)
	go waitForMasterEvents(nm)
	go waitForMemberEvents(nm)
	go waitForClusterMap(nm)
	JoinCluster(nm)
	nm.Wg.Wait()
}

//export hash ring
func GetClusterHashRing() *consistenthash.ConsistentHashRing {
	return nodemeta.HashRing
}
