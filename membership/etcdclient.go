package membership

import (
	"time"

	"github.com/coreos/etcd/clientv3"
)

func GetClient(Endpoints []string, DialTimeout time.Duration, reconnectcounter int) (*clientv3.Client, error) {
	retrytime := 0
	cfg := clientv3.Config{
		Endpoints:   Endpoints,
		DialTimeout: DialTimeout,
	}
RECONNECT:
	c, err := clientv3.New(cfg)
	if err != nil {
		if retrytime < reconnectcounter {
			retrytime++
			goto RECONNECT
		}
	}
	return c, err
}
