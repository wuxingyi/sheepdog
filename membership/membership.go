package membership

import (
	"fmt"
	"log"
	"time"

	"github.com/coreos/etcd/clientv3"

	"golang.org/x/net/context"
	"legitlab.letv.cn/wuxingyi/sheepdog/util"
)

const RetryCounter = 2
const JoinDuration = 5
const MemberZone = "/sheepdog/member/"
const PendingLeaving = "/sheepdog/pendingleaving/"
const ClusterMap = "/sheepdog/clustermap"

const (
	MESSAGE_NODE    = 1
	MESSAGE_CLUSTER = 2
)

//a master node is a node responsible for join/leaving events, a master should tell the newly joined node who are the other guys in the cluster
const MasterNode = "/sheepdog/master"

type Clusterinfo struct {
	Disable_recovery bool
	Nr_nodes         uint
	Epoch            uint
	Nr_copies        uint
	MasterID         util.SHEEPDOGNODE
}

type ClusterMessage struct {
	Ci    Clusterinfo
	Nodes []util.SHEEPDOGNODE //wuxingyi:Nodes already exist in this cluster
}

func JoinCluster(nodemeta *NodeMetaData) {
	myself := nodemeta.Myself
	c := nodemeta.Client
	lease := nodemeta.Lease
	//after sleep 1 second, put my self to member
	time.Sleep(time.Second)
	buf, encodeerr := util.Encode(MESSAGE_NODE, myself)
	if encodeerr != nil {
		log.Fatal(encodeerr)
	}

	_, err := c.Put(context.TODO(), MemberZone+fmt.Sprintf("%d", myself.NodeId), buf.String(), clientv3.WithLease(lease))
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("node %d: send join request successfully\r\n", myself.NodeId)
	go func() {
		//try to rejoin in case I try to join when no master is in power
		joined := false
		for i := 1; i < RetryCounter+1; i++ {
			//we sleep JoinDuration+1 seconds because if there are others nodes in the cluster, it's enough to elete a new master
			time.Sleep((JoinDuration + 1) * time.Second)
			nodemeta.JoinedLock.Lock()
			if nodemeta.IsJoined != true {
				_, err := c.Put(context.TODO(), MemberZone+fmt.Sprintf("%d", myself.NodeId), buf.String(), clientv3.WithLease(lease))
				if err != nil {
					log.Fatal(err)
				}
				log.Printf("node %d: resend join request successfully, this is the %dth retry\r\n", myself.NodeId, i)
			} else {
				joined = true
				nodemeta.JoinedLock.Unlock()
				break
			}
			nodemeta.JoinedLock.Unlock()
		}

		if joined == false {
			log.Printf("node %d: I have waiting for enough time, there may be no other members in the cluster, try to compete master\r\n", nodemeta.Myself.NodeId)
			win := CompeteMaster(nodemeta)
			if win != true {
				log.Printf("node %d: another guy already wins the election\r\n", nodemeta.Myself.NodeId)
			} else {
				log.Printf("node %d: I become the initial master now\r\n", nodemeta.Myself.NodeId)
			}
		}
	}()
}

func compete(c *clientv3.Client, lease clientv3.LeaseID, node *util.SHEEPDOGNODE) bool {
	buf, encodeerr := util.Encode(MESSAGE_NODE, node)
	if encodeerr != nil {
		log.Fatal(encodeerr)
	}

	//this is a transaction, if version!=0, it will not success
	req := clientv3.OpPut(MasterNode, buf.String(), clientv3.WithLease(lease))
	cond := clientv3.Compare(clientv3.Version(MasterNode), "=", 0)
	resp, err := c.Txn(context.TODO()).If(cond).Then(req).Commit()
	if err != nil {
		log.Fatal("faild to commit")
	}

	return resp.Succeeded
}

func maybeRemovePendingLeaving(nodemeta *NodeMetaData) {
	getresp, err := nodemeta.Client.Get(context.TODO(), PendingLeaving, clientv3.WithPrefix())
	if err != nil {
		log.Fatal("failed to get pendingleaving nodes, err is: ", err)
	}
	if getresp.Count > 0 {
		for _, nid := range getresp.Kvs {
			var nodeid uint
			fmt.Sscanf(string(nid.Value), "%d", &nodeid)
			//if node is not exists anymore, we can delete the pendingleaving node now
			if _, exists := nodemeta.NodeMap[nodeid]; exists != true {
				log.Printf("node %d: removed pending leaving node %d\r\n", nodemeta.Myself.NodeId, nodeid)
				nodemeta.Client.Delete(context.TODO(), PendingLeaving+fmt.Sprintf("%d", nodeid))
			}
		}
	}
}

//watch for member change with prefix MemberZone
func waitForMemberEvents(nodemeta *NodeMetaData) {
	defer nodemeta.Wg.Done()
	c := nodemeta.Client
	memberch := c.Watch(context.TODO(), MemberZone, clientv3.WithPrefix())
	for {
		select {
		case wresp := <-memberch:
			for _, ev := range wresp.Events {
				var nodes []util.SHEEPDOGNODE
				var epoch uint
				//log.Printf("we got a member event, type is %s\r\n", ev.Type.String())
				if ev.Type.String() == "DELETE" {
					nodemeta.MasterLock.Lock()
					if nodemeta.IsMaster == false {
						nodemeta.MasterLock.Unlock()

						//put this node to pendingleaving
						var leavingid uint
						fmt.Sscanf(string(ev.Kv.Key), MemberZone+"%d", &leavingid)
						log.Printf("node %d: put node %d to pending leaving", nodemeta.Myself.NodeId, leavingid)
						_, err := nodemeta.Client.Put(context.TODO(), PendingLeaving+fmt.Sprintf("%d", leavingid), fmt.Sprintf("%d", leavingid))
						if err != nil {
							log.Fatal(err)
						}
						continue
					}
					nodemeta.MasterLock.Unlock()

					//log.Println(string(ev.Kv.Key))
					if string(ev.Kv.Key) != MemberZone+fmt.Sprintf("%d", nodemeta.Myself.NodeId) { //someone left cluster
						nodemeta.CILock.Lock()
						//increase epoch when member event happens
						nodemeta.Ci.Epoch++
						epoch = nodemeta.Ci.Epoch
						//handle leaving event
						var id uint
						_, _ = fmt.Sscanf(string(ev.Kv.Key), MemberZone+"%d", &id)

						log.Printf("node %d leaved cluster\r\n", id)
						if toremovenode, ok := nodemeta.NodeMap[id]; ok == true {
							//remove node from hashring
							nodemeta.HashRing.Remove(&toremovenode)

							//delete from nodemap
							delete(nodemeta.NodeMap, id)
						}

						nodemeta.Ci.Nr_nodes--
						log.Printf("node %d: after leaving, we got %d nodes in this cluster, they are: \r\n", nodemeta.Myself.NodeId, nodemeta.Ci.Nr_nodes)
						for nodek, nodev := range nodemeta.NodeMap {
							nodes = append(nodes, nodev)
							log.Println(nodek, nodev.NodeId, nodev.NR_vnodes, nodev.Zone)
						}
						nodemeta.CILock.Unlock()

					} else {
						log.Fatal("this is unlikely to happen")
					}
				} else {
					nodemeta.MasterLock.Lock()
					if nodemeta.IsMaster == false {
						log.Printf("node %d: I don't need to process member join event because I'm not a master\r\n", nodemeta.Myself.NodeId)
						nodemeta.MasterLock.Unlock()
						continue
					}
					nodemeta.MasterLock.Unlock()
					//handle member join event
					var newnode util.SHEEPDOGNODE
					err := util.Decode(MESSAGE_NODE, ev.Kv.Value, &newnode)
					if err != nil {
						log.Fatal("failed to decode MESSAGE_NODE")
					}
					if newnode.NodeId == nodemeta.Myself.NodeId {
						//I have already joined myself
						continue
					}

					nodemeta.NodeMapLock.Lock()
					if _, exists := nodemeta.NodeMap[newnode.NodeId]; exists == true {
						log.Printf("node %d: node %d is already joined cluster\r\n", nodemeta.Myself.NodeId, newnode.NodeId)
						nodemeta.NodeMapLock.Unlock()
						continue
					}

					nodemeta.CILock.Lock()
					nodemeta.NodeMap[newnode.NodeId] = newnode
					//increase epoch when member event happens
					nodemeta.Ci.Epoch++
					epoch = nodemeta.Ci.Epoch
					nodemeta.Ci.Nr_nodes++

					//add myself to hashring
					nodemeta.HashRing.Add(&newnode)

					log.Printf("node %d: welcomes node %d successfully join cluster\r\n", nodemeta.Myself.NodeId, newnode.NodeId)
					log.Printf("node %d: after join node %d, we got %d nodes in this cluster, they are:\r\n", nodemeta.Myself.NodeId, newnode.NodeId, nodemeta.Ci.Nr_nodes)
					for _, nodev := range nodemeta.NodeMap {
						nodes = append(nodes, nodev)
						log.Printf("node %d: %d, %d, %d", nodemeta.Myself.NodeId, nodev.NodeId, nodev.NR_vnodes, nodev.Zone)
					}
					nodemeta.CILock.Unlock()
					nodemeta.NodeMapLock.Unlock()
				}
				//we should publish new cluster map now
				nodemeta.CILock.Lock()
				mastermessage := ClusterMessage{nodemeta.Ci, nodes}
				mmbuf, encodeerr := util.Encode(MESSAGE_CLUSTER, mastermessage)
				nodemeta.CILock.Unlock()

				if encodeerr != nil {
					log.Fatal(encodeerr)
				}

				_, err := c.Put(context.TODO(), ClusterMap, mmbuf.String())
				if err != nil {
					log.Fatal(err)
				}
				log.Printf("node %d: a cluster map with epoch %d is published to etcd\r\n", nodemeta.Myself.NodeId, epoch)
			}
		}
	}
}

//wait for master events
func waitForMasterEvents(nodemeta *NodeMetaData) {
	defer nodemeta.Wg.Done()
	c := nodemeta.Client
	lease := nodemeta.Lease
	masterch := c.Watch(context.TODO(), MasterNode)
	for {
		select {
		case mresp := <-masterch:
			for _, ev := range mresp.Events {
				if ev.Type.String() == "DELETE" {
					nodemeta.JoinedLock.Lock()
					if nodemeta.IsJoined != true {
						//not joined yet, don't compete master
						log.Printf("node %d: master died, but I'm not joined yet, so don't compete master\r\n", nodemeta.Myself.NodeId)
						nodemeta.JoinedLock.Unlock()
						continue
					}
					nodemeta.JoinedLock.Unlock()

					log.Printf("node %d: master died, compete for master\r\n", nodemeta.Myself.NodeId)
					if compete(c, lease, nodemeta.Myself) == true {
						log.Printf("node %d: I'm the master now, publishing cluster map", nodemeta.Myself.NodeId)

						//we should try to rebuild node list because between I become master, some nodes may have left cluster
						nodemeta.MasterLock.Lock()
						nodemeta.IsMaster = true
						nodemeta.MasterLock.Unlock()

						oldmaster := nodemeta.NodeMap[nodemeta.Ci.MasterID.NodeId]

						//remove oldmaster from hashring
						nodemeta.HashRing.Remove(&oldmaster)

						//delete old master from nodemap
						delete(nodemeta.NodeMap, nodemeta.Ci.MasterID.NodeId)

						getresp, err := nodemeta.Client.Get(context.TODO(), PendingLeaving, clientv3.WithPrefix())
						if err != nil {
							log.Fatal("failed to get pendingleaving nodes, err is: ", err)
						}
						if getresp.Count > 0 {
							for _, nid := range getresp.Kvs {
								var nodeid uint
								fmt.Sscanf(string(nid.Value), "%d", &nodeid)

								//if node is exists in our node list, but in pendingleaving nodes, we should delete the pendingleaving node now
								if toremovenode, exists := nodemeta.NodeMap[nodeid]; exists == true {
									log.Printf("node %d: removed pending leaving node %d\r\n", nodemeta.Myself.NodeId, nodeid)
									//remove node from hashring
									nodemeta.HashRing.Remove(&toremovenode)

									delete(nodemeta.NodeMap, nodeid)
									nodemeta.Client.Delete(context.TODO(), PendingLeaving+fmt.Sprintf("%d", nodeid))
									nodemeta.Ci.Nr_nodes--
								}
							}
						}

						//we should publish new cluster map now
						nodemeta.CILock.Lock()
						nodemeta.Ci.MasterID = *nodemeta.Myself
						var nodes []util.SHEEPDOGNODE
						for _, nodev := range nodemeta.NodeMap {
							log.Printf("node %d: %d, %d, %d", nodemeta.Myself.NodeId, nodev.NodeId, nodev.NR_vnodes, nodev.Zone)
							nodes = append(nodes, nodev)
						}

						nodemeta.Ci.Epoch++
						nodemeta.Ci.Nr_nodes--
						mastermessage := ClusterMessage{nodemeta.Ci, nodes}
						mmbuf, encodeerr := util.Encode(MESSAGE_CLUSTER, mastermessage)
						nodemeta.CILock.Unlock()

						if encodeerr != nil {
							log.Fatal(encodeerr)
						}

						_, puterr := c.Put(context.TODO(), ClusterMap, mmbuf.String())
						if puterr != nil {
							log.Fatal(puterr)
						}
					} else {
						log.Printf("node %d: failed to be master\r\n", nodemeta.Myself.NodeId)
					}
				} else {
					//we got a new leader now
					var newmaster util.SHEEPDOGNODE
					err := util.Decode(MESSAGE_NODE, ev.Kv.Value, &newmaster)
					if err != nil {
						log.Fatal("failed to decode MESSAGE_NODE")
					}
					if newmaster.NodeId != nodemeta.Myself.NodeId {
						log.Printf("node %d: congrats to node %d\r\n", nodemeta.Myself.NodeId, newmaster.NodeId)
					} else {
						log.Printf("node %d: I'm the master now", nodemeta.Myself.NodeId)
					}
				}
			}
		}
	}
}

//wait for clustermap
func waitForClusterMap(nodemeta *NodeMetaData) {
	defer nodemeta.Wg.Done()
	c := nodemeta.Client
	mapch := c.Watch(context.TODO(), ClusterMap)
	for {
		select {
		case mresp := <-mapch:
			for _, ev := range mresp.Events {
				log.Printf("node %d: we got new cluster map", nodemeta.Myself.NodeId)
				nodemeta.MasterLock.Lock()
				if nodemeta.IsMaster == true {
					nodemeta.MasterLock.Unlock()
					//it's master's duty to remove pendingleaving nodes
					log.Printf("node %d: maybe need to remove pending leaving nodes\r\n", nodemeta.Myself.NodeId)
					maybeRemovePendingLeaving(nodemeta)

					continue
				}
				nodemeta.MasterLock.Unlock()

				var cm ClusterMessage
				derr := util.Decode(MESSAGE_CLUSTER, ev.Kv.Value, &cm)
				if derr != nil {
					log.Fatal("failed to decode MESSAGE_CLUSTER")
				}

				nodemeta.CILock.Lock()
				if cm.Ci.Epoch <= nodemeta.Ci.Epoch {
					//unlikely to happen
					log.Fatal("got a old version of cluster map")
				}
				log.Printf("node %d: we got new cluster map, epoch is %d", nodemeta.Myself.NodeId, cm.Ci.Epoch)

				//copy master's ci to my ci
				nodemeta.Ci = cm.Ci

				tempmap := make(map[uint]util.SHEEPDOGNODE)
				nodemeta.JoinedLock.Lock()

				//remove all nodes and add nodes from cluster map
				//(FIXME)this is really ugly
				nodemeta.HashRing.RemoveAllNodes()
				for _, node := range cm.Nodes {
					tempmap[node.NodeId] = node
					if node.NodeId == nodemeta.Myself.NodeId && nodemeta.IsJoined == false {
						nodemeta.IsJoined = true
						log.Printf("node %d: I was joined to cluster at epoch %d\r\n", nodemeta.Myself.NodeId, nodemeta.Ci.Epoch)
					}

					log.Printf("node %d: add node %d to hash ring\r\n", nodemeta.Myself.NodeId, node.NodeId)
					nodemeta.HashRing.Add(&node)
				}
				nodemeta.JoinedLock.Unlock()

				nodemeta.NodeMap = tempmap
				log.Printf("node %d: at epoch %d, we got following nodes: \r\n", nodemeta.Myself.NodeId, nodemeta.Ci.Epoch)
				for _, nodev := range nodemeta.NodeMap {
					log.Printf("node %d: %d, %d, %d", nodemeta.Myself.NodeId, nodev.NodeId, nodev.NR_vnodes, nodev.Zone)
				}
				nodemeta.CILock.Unlock()
			}
		}
	}
}

//trying for leadership, current implemetion tries to mantain a steady leadership
func CompeteMaster(nodemeta *NodeMetaData) bool {
	//a newly joined node should not compete master
	//the first node in the cluster should be treated differently
	c := nodemeta.Client
	lease := nodemeta.Lease
	if compete(c, lease, nodemeta.Myself) == true {
		//set metadata
		nodemeta.MasterLock.Lock()
		nodemeta.IsMaster = true
		nodemeta.MasterLock.Unlock()

		nodemeta.JoinedLock.Lock()
		nodemeta.IsJoined = true
		nodemeta.JoinedLock.Unlock()

		var originalepoch uint = 1
		//first check my existency
		getresp, _ := c.Get(context.TODO(), ClusterMap)
		if getresp.Count > 0 {
			var cm ClusterMessage
			derr := util.Decode(MESSAGE_CLUSTER, getresp.Kvs[0].Value, &cm)
			if derr != nil {
				log.Fatal("failed to decode MESSAGE_CLUSTER")
			}
			originalepoch = cm.Ci.Epoch + originalepoch
		}

		nodemeta.CILock.Lock()
		nodemeta.Ci.Epoch = originalepoch
		nodemeta.Ci.Nr_nodes++
		nodemeta.CILock.Unlock()

		nodemeta.NodeMapLock.Lock()
		nodemeta.NodeMap[nodemeta.Myself.NodeId] = *nodemeta.Myself
		nodemeta.Ci.MasterID = *nodemeta.Myself
		nodemeta.NodeMapLock.Unlock()

		//add myself to hashring
		nodemeta.HashRing.Add(nodemeta.Myself)
		return true
	} else {
		return false
	}
}
