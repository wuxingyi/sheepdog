package membership

import (
	"context"
	"fmt"
	"log"
	"testing"
	"time"

	"legitlab.letv.cn/wuxingyi/sheepdog/util"

	"github.com/coreos/etcd/clientv3"
)

const SHEEPDOGKEYPREFIX = "/sheepdog"
const DEFAULTLEASE = 5

//all test cases should run a etcd daemon at 127.0.0.1:2379
func TestConnect(t *testing.T) {
	_, err := GetClient([]string{"127.0.0.1:2379"}, time.Second, 1)
	if err != nil {
		t.Error("failed to connect cluster")
	}
}

//clear all cluster info to key env clean
func clearETCD() int64 {
	c, _ := GetClient([]string{"127.0.0.1:2379"}, time.Second, 1)
	c.Delete(context.TODO(), SHEEPDOGKEYPREFIX, clientv3.WithPrefix())

	getresp, _ := c.Get(context.TODO(), SHEEPDOGKEYPREFIX, clientv3.WithPrefix())
	c.Close()
	return getresp.Count
}

func TestClearETCD(t *testing.T) {
	if clearETCD() != 0 {
		t.Error("faild to clear etcd env")
	}
}

func TestWinStandaloneElection(t *testing.T) {
	clearETCD()
	masternm := initNodeMeta(222, 222, 222, DEFAULTLEASE, "", "")
	success := compete(masternm.Client, masternm.Lease, masternm.Myself)
	if success != true {
		t.Error("failed to win a standalone election")
	}
	masternm.Client.Close()
	//gracefully kill clients
	time.Sleep(time.Second)
}

//this may be time-consuming, because we should elect a leader
//the new election design doesn't allow a newly joined node become master until it find it's impossible to find a master.
func TestJoinWithoutElectedMaster(t *testing.T) {
	clearETCD()

	//master win election
	masternm := initNodeMeta(222, 222, 222, DEFAULTLEASE, "", "")
	JoinCluster(masternm)
	go waitForClusterMap(masternm)
	go waitForMasterEvents(masternm)
	go waitForMemberEvents(masternm)

	//a new node try to join cluster
	newnodenm := initNodeMeta(111, 111, 111, DEFAULTLEASE, "", "")
	go waitForClusterMap(newnodenm)
	go waitForMasterEvents(newnodenm)
	go waitForMemberEvents(newnodenm)

	JoinCluster(newnodenm)
	time.Sleep(time.Second * (DEFAULTLEASE + 1) * 3)
	newnodenm.JoinedLock.Lock()
	if newnodenm.IsJoined != true {
		t.Error("Node Join Failed")
	}
	newnodenm.JoinedLock.Unlock()

	newnodenm.Client.Close()
	time.Sleep(time.Second)
	masternm.Client.Close()
	time.Sleep(time.Second)
}

func TestMasterDie(t *testing.T) {
	clearETCD()

	var i uint = 0
	var master *NodeMetaData

	nodemap := make(map[uint]*NodeMetaData)

	//master win election
	for ; i < 2; i++ {
		nm := initNodeMeta(i, i, i, DEFAULTLEASE, "", "")
		nodemap[i] = nm
		success := CompeteMaster(nm)
		if success == true {
			master = nm
		}

		go waitForClusterMap(nm)
		go waitForMasterEvents(nm)
		go waitForMemberEvents(nm)
	}

	time.Sleep(time.Second)
	master.Client.Close()
	master.MasterLock.Lock()
	if master.IsMaster != true {
		t.Error("failed to be master when master died")
	}
	master.MasterLock.Unlock()

	//kill nodes gracefully
	nodemap[1].Client.Close()
	time.Sleep(time.Second)
	nodemap[0].Client.Close()
	time.Sleep(time.Second)
}

func TestCompeteMaster(t *testing.T) {
	clearETCD()
	nodemap := make(map[uint]*NodeMetaData)

	var i uint = 0
	mastervector := make([]bool, 10)

	//master win election
	for ; i < 10; i++ {
		masternm := initNodeMeta(i, i, i, DEFAULTLEASE, "", "")
		nodemap[i] = masternm
		success := CompeteMaster(masternm)
		if success == true {
			mastervector[i] = true
		}
		go waitForClusterMap(masternm)
		go waitForMasterEvents(masternm)
		go waitForMemberEvents(masternm)
	}

	winnernr := 0
	for id, j := range mastervector {
		if j == true {
			winnernr++
			log.Printf("winner is %d\r\n", id)
		}
	}

	if winnernr != 1 {
		t.Error("more than one master for a single election")
	}

	for i = 1; i < 10; i++ {
		nodemap[i].Client.Close()
		time.Sleep(time.Second)
	}
	nodemap[0].Client.Close()
	time.Sleep(time.Second * 20)
}

func TestConcurrentJoin(t *testing.T) {
	clearETCD()

	var i uint = 0
	var needjoin uint = 4
	var leader *NodeMetaData
	var nmvec []*NodeMetaData

	//master win election
	for ; i < needjoin; i++ {
		go func(j uint) {
			masternm := initNodeMeta(j, j, j, DEFAULTLEASE, "", "")
			nmvec = append(nmvec, masternm)
			go waitForClusterMap(masternm)
			go waitForMasterEvents(masternm)
			go waitForMemberEvents(masternm)
			success := CompeteMaster(masternm)
			if success == true {
				leader = masternm
			} else {
				JoinCluster(masternm)
			}
		}(i)
	}

	//wait the last node join, JoinDuration is possiblely enough
	time.Sleep(time.Second * JoinDuration)
	leader.CILock.Lock()
	if leader.Ci.Nr_nodes != needjoin {
		t.Errorf("node number is not correct, we got %d", leader.Ci.Nr_nodes)
	}
	leader.CILock.Unlock()

	for index, i := range nmvec {
		if index > 0 {
			i.Client.Close()
			time.Sleep(time.Second)
		}
	}
	nmvec[0].Client.Close()
}
func TestSeqJoin(t *testing.T) {
	clearETCD()
	//waiting for previous testcase close etcd clients
	nodemap := make(map[uint]*NodeMetaData)
	var i uint = 0
	var needjoin uint = 3
	var leader *NodeMetaData

	//master win election
	for ; i < needjoin; i++ {
		masternm := initNodeMeta(i, i, i, DEFAULTLEASE, "", "")
		go waitForClusterMap(masternm)
		go waitForMasterEvents(masternm)
		go waitForMemberEvents(masternm)
		success := CompeteMaster(masternm)
		if success == true {
			leader = masternm
		} else {
			JoinCluster(masternm)
		}
		nodemap[i] = masternm

		//give it a second then join next node
		time.Sleep(2 * time.Second)
	}

	//wait the last node join, JoinDuration is possiblely enough
	time.Sleep(time.Second * JoinDuration)
	leader.CILock.Lock()
	if leader.Ci.Nr_nodes != needjoin {
		t.Errorf("node number is not correct, we got %d", leader.Ci.Nr_nodes)
	}
	leader.CILock.Unlock()

	time.Sleep(time.Second * 20)
	for i = 0; i < needjoin; i++ {
		nodemap[i].Client.Close()
		time.Sleep(time.Second)
	}
}

func TestSeqLeave(t *testing.T) {
	clearETCD()
	var nmvec []*NodeMetaData

	var i uint = 0
	var needjoin uint = 4
	var leader *NodeMetaData

	//master win election
	for ; i < needjoin; i++ {
		masternm := initNodeMeta(i, i, i, DEFAULTLEASE, "", "")
		go waitForClusterMap(masternm)
		go waitForMasterEvents(masternm)
		go waitForMemberEvents(masternm)
		success := CompeteMaster(masternm)
		if success == true {
			leader = masternm
		} else {
			JoinCluster(masternm)
			nmvec = append(nmvec, masternm)
		}
		time.Sleep(time.Second)
	}

	//wait the last node join, JoinDuration is possiblely enough
	time.Sleep(time.Second * JoinDuration)
	leader.CILock.Lock()
	if leader.Ci.Nr_nodes != needjoin {
		t.Errorf("after join, node number is not correct, we got %d", leader.Ci.Nr_nodes)
	}
	leader.CILock.Unlock()

	for _, i := range nmvec {
		time.Sleep(time.Second)
		i.Client.Close()
	}

	//waiting for cluster map propagate
	time.Sleep(time.Second * JoinDuration * 2)
	leader.CILock.Lock()
	if leader.Ci.Nr_nodes != 1 {
		t.Errorf("after leaving, node number is not correct, we got %d", leader.Ci.Nr_nodes)
	}
	leader.CILock.Unlock()
	leader.Client.Close()
}

func TestConcurrentLeave(t *testing.T) {
	clearETCD()
	//waiting for previous testcase clean its env
	//time.Sleep(time.Second * JoinDuration * 2)
	t.Log("etcd env cleaned")
	var nmvec []*NodeMetaData

	var i uint = 0
	var needjoin uint = 5
	var leader *NodeMetaData

	//master win election
	for ; i < needjoin; i++ {
		masternm := initNodeMeta(i, i, i, DEFAULTLEASE, "", "")
		go waitForClusterMap(masternm)
		go waitForMasterEvents(masternm)
		go waitForMemberEvents(masternm)
		success := CompeteMaster(masternm)
		if success == true {
			leader = masternm
		} else {
			JoinCluster(masternm)
			nmvec = append(nmvec, masternm)
		}
	}

	//wait the last node join, JoinDuration is possiblely enough
	time.Sleep(time.Second * JoinDuration)
	leader.CILock.Lock()
	if leader.Ci.Nr_nodes != needjoin {
		t.Errorf("after join, node number is not correct, we got %d", leader.Ci.Nr_nodes)
	} else {
		t.Logf("all %d nodes joined", leader.Ci.Nr_nodes)
	}
	leader.CILock.Unlock()

	for _, i := range nmvec {
		i.Client.Close()
	}

	//waiting for cluster map propagate
	time.Sleep(time.Second * 20)
	leader.CILock.Lock()
	if leader.Ci.Nr_nodes != 1 {
		t.Errorf("after leaving, node number is not correct, we got %d", leader.Ci.Nr_nodes)
	}
	leader.CILock.Unlock()
	leader.Client.Close()
}

//leave when there is no master exists
func TestLeaveWithoutMaster(t *testing.T) {
	clearETCD()
	time.Sleep(time.Second * 20)
	nodemap := make(map[uint]*NodeMetaData)

	var i uint = 0
	var needjoin uint = 10
	var masternm *NodeMetaData

	//master win election
	for ; i < needjoin; i++ {
		if i == 0 {
			masternm = initNodeMeta(i, i, i, DEFAULTLEASE*10, "", "")
		} else {
			masternm = initNodeMeta(i, i, i, DEFAULTLEASE, "", "")
		}
		go waitForClusterMap(masternm)
		go waitForMasterEvents(masternm)
		go waitForMemberEvents(masternm)
		success := CompeteMaster(masternm)
		if success != true {
			JoinCluster(masternm)
		}
		nodemap[i] = masternm
	}

	if nodemap[0].Myself.NodeId != 0 {
		t.Error("node 0 should be master")
	}

	time.Sleep(time.Second * JoinDuration)
	for i, n := range nodemap {
		if i != needjoin-1 {
			n.Client.Close()
		}
	}

	//waiting for cluster map messaging, node needjoin-1 become master
	time.Sleep(time.Second * 10 * 3)
	leader := nodemap[needjoin-1]
	leader.CILock.Lock()
	if leader.Ci.Nr_nodes != 1 {
		t.Errorf("after leaving, node number is not correct, we got %d", leader.Ci.Nr_nodes)
	}
	leader.CILock.Unlock()
	leader.Client.Close()
	time.Sleep(time.Second * 30)
}

//leave and join when there is no master exists
func TestLeaveAndJoinWithoutMaster(t *testing.T) {
	clearETCD()
	//waiting for previous testcase close etcd clients
	nodemap := make(map[uint]*NodeMetaData)

	var i uint = 0
	var needjoin uint = 10
	var masternm *NodeMetaData

	//master win election
	for ; i < needjoin; i++ {
		if i == 0 {
			masternm = initNodeMeta(i, i, i, DEFAULTLEASE*10, "", "")
		} else {
			masternm = initNodeMeta(i, i, i, DEFAULTLEASE, "", "")
		}
		go waitForClusterMap(masternm)
		go waitForMasterEvents(masternm)
		go waitForMemberEvents(masternm)
		success := CompeteMaster(masternm)
		if success != true {
			JoinCluster(masternm)
		}
		nodemap[i] = masternm
	}

	if nodemap[0].Myself.NodeId != 0 {
		t.Error("node 0 should be master")
	}

	time.Sleep(time.Second * JoinDuration)
	for i, n := range nodemap {
		if i != needjoin-1 {
			n.Client.Close()
		}
	}

	//after killing nodes, also add one node
	var joinid uint = 1000
	masternm = initNodeMeta(joinid, joinid, joinid, DEFAULTLEASE*10, "", "")
	go waitForClusterMap(masternm)
	go waitForMasterEvents(masternm)
	go waitForMemberEvents(masternm)

	JoinCluster(masternm)
	//waiting for cluster map messaging, node needjoin-1 become master
	time.Sleep(time.Second * 10 * 2)
	leader := nodemap[needjoin-1]
	leader.CILock.Lock()
	if leader.Ci.Nr_nodes != 2 {
		t.Errorf("after leaving, node number is not correct, we got %d", leader.Ci.Nr_nodes)
	}
	leader.CILock.Unlock()

	leader.Client.Close()
	masternm.Client.Close()
	time.Sleep(time.Second * 20)
}

//test whether epoch growing steadyly
func TestGrowingEpoch(t *testing.T) {
	clearETCD()
	var i uint = 0
	var totalnodes uint = 5

	nodemap := make(map[uint]*NodeMetaData)

	//master win election
	for ; i < totalnodes; i++ {
		nm := initNodeMeta(i, i, i, DEFAULTLEASE, "", "")
		go waitForClusterMap(nm)
		go waitForMasterEvents(nm)
		go waitForMemberEvents(nm)
		nodemap[i] = nm
		success := CompeteMaster(nm)
		if success != true {
			JoinCluster(nm)
		}
	}

	//give nodes 10 secs to join
	time.Sleep(time.Second * 10)

	for i = 0; i < totalnodes; i++ {
		nodemap[i].Client.Close()
	}

	//give nodes 10 secs to leave
	time.Sleep(time.Second * 10)

	masternm := initNodeMeta(222, 222, 222, DEFAULTLEASE, "", "")
	success := CompeteMaster(masternm)
	if success != true {
		t.Error("failed to win a standalone election")
	}
	masternm.CILock.Lock()
	if masternm.Ci.Epoch <= 1 {
		t.Error("epoch should be great than 1")
	} else {
		t.Logf("epoch is %d", masternm.Ci.Epoch)
	}
	masternm.CILock.Unlock()

	masternm.Client.Close()
	//gracefully kill clients
	time.Sleep(time.Second)
}

func TestLocateObject(t *testing.T) {
	clearETCD()
	//waiting for previous testcase close etcd clients
	nodemap := make(map[uint]*NodeMetaData)
	var i uint = 0
	var needjoin uint = 10
	var leader *NodeMetaData
	copies := 4

	//master win election
	for ; i < needjoin; i++ {
		si := fmt.Sprintf("%d", i)
		masternm := initNodeMeta(i, 8, i, DEFAULTLEASE, "127.0.0."+si, "808"+si)
		go waitForClusterMap(masternm)
		go waitForMasterEvents(masternm)
		go waitForMemberEvents(masternm)
		success := CompeteMaster(masternm)
		if success == true {
			leader = masternm
		} else {
			JoinCluster(masternm)
		}
		nodemap[i] = masternm
	}

	//wait the last node join, JoinDuration is possiblely enough
	time.Sleep(time.Second * JoinDuration)
	leader.CILock.Lock()
	if leader.Ci.Nr_nodes != needjoin {
		t.Errorf("node number is not correct, we got %d", leader.Ci.Nr_nodes)
	}
	leader.CILock.Unlock()

	var j uint
	objectmap := make(map[uint][]*util.SHEEPDOGNODE)
	for j = 0; j < 2; j++ {
		node := nodemap[j]
		key := "hehe"
		nodes, err := node.HashRing.HashToNodes(key, copies)
		if err != nil {
			t.Error("failed to get object location, err is ", err)
		} else {
			for i := 0; i < copies; i++ {
				fmt.Print(nodes[i], " ")
			}
		}

		fmt.Println()
		objectmap[j] = nodes
	}

	if len(objectmap[0]) != len(objectmap[1]) {
		t.Error("hash result length mismatch")
	}

	for k := 0; k < len(objectmap[0]); k++ {
		if objectmap[0][k].NodeId != objectmap[1][k].NodeId {
			t.Error("hash result value mismatch")
		}
	}

	for i = 0; i < needjoin; i++ {
		nodemap[i].Client.Close()
	}
	time.Sleep(time.Second * 20)
}

func TestLocateZone(t *testing.T) {
	clearETCD()
	//waiting for previous testcase close etcd clients
	nodemap := make(map[uint]*NodeMetaData)
	var i uint = 0
	var needjoin uint = 20
	var leader *NodeMetaData
	copies := 3
	var z uint = 0

	//master win election
	for ; i < needjoin; i++ {
		if z%3 == 1 {
			z++
		}

		si := fmt.Sprintf("%d", i)
		masternm := initNodeMeta(i, 32, z, DEFAULTLEASE, "127.0.0."+si, "808"+si)
		go waitForClusterMap(masternm)
		go waitForMasterEvents(masternm)
		go waitForMemberEvents(masternm)
		success := CompeteMaster(masternm)
		if success == true {
			leader = masternm
		} else {
			JoinCluster(masternm)
		}
		nodemap[i] = masternm
		z++
	}

	//wait the last node join, JoinDuration is possiblely enough
	time.Sleep(time.Second * JoinDuration)
	leader.CILock.Lock()
	if leader.Ci.Nr_nodes != needjoin {
		t.Errorf("node number is not correct, we got %d", leader.Ci.Nr_nodes)
	}
	leader.CILock.Unlock()

	var j uint
	objectmap := make(map[uint][]*util.SHEEPDOGNODE)
	for j = 0; j < needjoin; j++ {
		node := nodemap[j]
		key := "hehe"
		nodes, err := node.HashRing.HashToNodes(key, copies)
		if err != nil {
			t.Error("failed to get object location, err is ", err)
		}
		objectmap[j] = nodes
	}

	l := len(objectmap[0])
	for i := range objectmap {
		if len(objectmap[i]) != l {
			t.Error("hash result length mismatch")
		}
	}

	for k := 0; k < len(objectmap[0]); k++ {
		for i = 0; i < needjoin-1; i++ {
			//the result nodes should totally equal
			if *objectmap[i][k] != *objectmap[i+1][k] {
				t.Error("value result length mismatch")
			}
		}
	}

	for i = 0; i < needjoin; i++ {
		nodemap[i].Client.Close()
	}
	time.Sleep(time.Second * 20)
}
