package consistenthash

import (
	"errors"
	"hash/crc32"
	"sort"
	"strconv"
	"sync"

	"legitlab.letv.cn/wuxingyi/sheepdog/util"
)

type HashRing []uint32

func (c HashRing) Len() int {
	return len(c)
}
func (c HashRing) Less(i, j int) bool {
	return c[i] < c[j]
}
func (c HashRing) Swap(i, j int) {
	c[i], c[j] = c[j], c[i]
}

type ConsistentHashRing struct {
	Nodes     map[uint32]*util.SHEEPDOGNODE
	Resources map[uint]bool
	ring      HashRing
	sync.RWMutex
}

func NewConsistentHashRing() *ConsistentHashRing {
	return &ConsistentHashRing{
		Nodes:     make(map[uint32]*util.SHEEPDOGNODE),
		Resources: make(map[uint]bool),
		ring:      HashRing{},
	}
}

func (c *ConsistentHashRing) Add(node *util.SHEEPDOGNODE) bool {
	c.Lock()
	defer c.Unlock()
	if _, ok := c.Resources[node.NodeId]; ok {
		return false
	}
	count := node.NR_vnodes
	var i uint = 0

	//(FIXME)I don't know why we need a new variable here, but it works now
	var newnode util.SHEEPDOGNODE = *node

	for ; i < count; i++ {
		str := c.joinStr(i, &newnode)
		c.Nodes[c.hashStr(str)] = &newnode
	}
	c.Resources[newnode.NodeId] = true
	c.sortHashRing()
	return true
}
func (c *ConsistentHashRing) sortHashRing() {
	c.ring = HashRing{}
	for k := range c.Nodes {
		c.ring = append(c.ring, k)
	}
	sort.Sort(c.ring)
}
func (c *ConsistentHashRing) joinStr(i uint, node *util.SHEEPDOGNODE) string {
	return node.Ip + "*" + "-" + strconv.Itoa(int(i)) + "-" + strconv.Itoa(int(node.NodeId))
}

//simple hash use crc32
func (c *ConsistentHashRing) hashStr(key string) uint32 {
	return crc32.ChecksumIEEE([]byte(key))
}

func (c *ConsistentHashRing) HashToFirstNode(key string) *util.SHEEPDOGNODE {
	c.RLock()
	defer c.RUnlock()
	hash := c.hashStr(key)
	i := c.search(hash)
	return c.Nodes[c.ring[i]]
}

//get @replicas nodes
func (c *ConsistentHashRing) HashToNodes(key string, copies int) (nodes []*util.SHEEPDOGNODE, err error) {
	c.RLock()
	defer c.RUnlock()
	hash := c.hashStr(key)
	i := c.search(hash)
	maybedeadloop := 0

	nodes = append(nodes, c.Nodes[c.ring[i]])

	replicas := copies - 1

	for replicas > 0 {
		//use maybedeadloop to avoid from 0 again and again
		if maybedeadloop > 1 {
			break
		}
		//when we hit the last element, we start again from 0
		if i == len(c.ring)-1 {
			i = 0
			maybedeadloop++
		} else {
			i++
		}
		newnode := c.Nodes[c.ring[i]]
		available := true
		for _, existsnodes := range nodes {
			if newnode.Zone == existsnodes.Zone {
				available = false
				break
			}
		}
		if available == true {
			nodes = append(nodes, newnode)
			replicas--
		}
	}

	if len(nodes) != copies {
		err = errors.New("can't got enouch copies")
	}
	return
}

//seach by hash
func (c *ConsistentHashRing) search(hash uint32) int {
	i := sort.Search(len(c.ring), func(i int) bool { return c.ring[i] >= hash })
	if i < len(c.ring) {
		if i == len(c.ring)-1 {
			return 0
		} else {
			return i
		}
	} else {
		return len(c.ring) - 1
	}
}

func (c *ConsistentHashRing) Remove(node *util.SHEEPDOGNODE) {
	c.Lock()
	defer c.Unlock()
	if _, ok := c.Resources[node.NodeId]; !ok {
		return
	}
	delete(c.Resources, node.NodeId)
	count := node.NR_vnodes
	var i uint = 0
	for ; i < count; i++ {
		str := c.joinStr(i, node)
		delete(c.Nodes, c.hashStr(str))
	}
	c.sortHashRing()
}

func (c *ConsistentHashRing) RemoveAllNodes() {
	c.Lock()
	defer c.Unlock()
	c.Nodes = nil
	c.Resources = nil

	c.Nodes = make(map[uint32]*util.SHEEPDOGNODE)
	c.Resources = make(map[uint]bool)
	c.sortHashRing()
}
