package consistenthash

import (
	"fmt"
	"testing"

	"legitlab.letv.cn/wuxingyi/sheepdog/util"
)

//this is a simple test, we will add performance test laterly
func TestSimple(t *testing.T) {
	objects := 10000
	cHashRing := NewConsistentHashRing()
	var i, z uint
	for i = 0; i < 100; i++ {
		si := fmt.Sprintf("%d", i)
		//each node has a different zone
		cHashRing.Add(util.NewSheepdogNode(i, 128, z, "172.18.1."+si, "8080"))
		if i%2 == 1 {
			z++
		}
	}

	for i := 0; i < objects; i++ {
		key := "hehe" + fmt.Sprintf("%d", i)
		nodes, err := cHashRing.HashToNodes(key, 3)
		if err != nil {
			fmt.Println(err)
		}

		fmt.Print(key, ":")
		for _, v := range nodes {
			fmt.Print(v.NodeId, " ")
		}
		fmt.Println()
	}

	if _, ok := cHashRing.Nodes[cHashRing.ring[0]]; ok {
		fmt.Printf("node %d has been removed\r\n", cHashRing.Nodes[cHashRing.ring[0]].NodeId)
		cHashRing.Remove(cHashRing.Nodes[cHashRing.ring[0]])
	} else {
		fmt.Println("no such node")
		return
	}

	fmt.Println("after removing:")

	for i := 0; i < objects; i++ {
		key := "hehe" + fmt.Sprintf("%d", i)
		nodes, err := cHashRing.HashToNodes(key, 3)
		if err != nil {
			fmt.Println(err)
		}

		fmt.Print(key, ":")
		for _, v := range nodes {
			fmt.Print(v.NodeId, " ")
		}
		fmt.Println()
	}

}

func TestSameNodeEqual(t *testing.T) {
	cHashRing1 := NewConsistentHashRing()
	cHashRing2 := NewConsistentHashRing()

	var i uint
	for i = 0; i < 10; i++ {
		si := fmt.Sprintf("%d", i)
		//each node has a different zone
		newnode := util.NewSheepdogNode(i, 16, i, "172.0.0."+si, "8080")
		cHashRing1.Add(newnode)
		cHashRing2.Add(newnode)
	}

	fmt.Println("finished creating tree")
	key := "hehe"
	nodes1, err := cHashRing1.HashToNodes(key, 3)
	if err != nil {
		fmt.Println(err)
	}

	nodes2, err := cHashRing2.HashToNodes(key, 3)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(nodes1)
	fmt.Println(nodes2)

	if len(nodes1) != len(nodes2) {
		t.Error("length mismatch")
	}

	for i := 0; i < len(nodes1); i++ {
		if nodes1[i].NodeId != nodes2[i].NodeId {
			t.Error("element mismatch")
		}
	}
}

func TestDifferntNodeEqual(t *testing.T) {
	cHashRing1 := NewConsistentHashRing()
	cHashRing2 := NewConsistentHashRing()

	var i uint
	for i = 0; i < 10; i++ {
		si := fmt.Sprintf("%d", i)
		//each node has a different zone
		cHashRing1.Add(util.NewSheepdogNode(i, 16, i, "172.0.0."+si, "8080"))
		cHashRing2.Add(util.NewSheepdogNode(i, 16, i, "172.0.0."+si, "8080"))
	}

	fmt.Println("finished creating tree")

	key := "hehe"
	nodes1, err := cHashRing1.HashToNodes(key, 3)
	if err != nil {
		fmt.Println(err)
	}

	nodes2, err := cHashRing2.HashToNodes(key, 3)
	if err != nil {
		fmt.Println(err)
	}

	if len(nodes1) != len(nodes2) {
		t.Error("length mismatch")
	}

	for i := 0; i < len(nodes1); i++ {
		if nodes1[i].NodeId != nodes2[i].NodeId {
			t.Error("element mismatch")
		}
	}
}
