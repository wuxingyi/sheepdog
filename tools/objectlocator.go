package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"legitlab.letv.cn/wuxingyi/sheepdog/consistenthash"
	"legitlab.letv.cn/wuxingyi/sheepdog/membership"
	"legitlab.letv.cn/wuxingyi/sheepdog/util"
)

var Objectname = flag.String("name", "hehe", "the object name you want to locate")
var Copies = flag.Int("copies", 3, "how many copies you want to locate")
var Watch = flag.Bool("watch", false, "do you want to watch on location change")

var ring *consistenthash.ConsistentHashRing

var epoch uint

//this tool is used to locate objects by latest cluster map
func main() {
	flag.Parse()
	c, err := membership.GetClient([]string{"127.0.0.1:2379"}, 5*time.Second, 2)

	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	getresp, _ := c.Get(context.TODO(), membership.ClusterMap)
	if getresp.Count > 0 {
		var cm membership.ClusterMessage
		derr := util.Decode(membership.MESSAGE_CLUSTER, getresp.Kvs[0].Value, &cm)
		if derr != nil {
			fmt.Println("failed to decode cluster map")
			os.Exit(-1)
		}
		epoch = cm.Ci.Epoch
		fmt.Printf("at epoch %d:\r\n", epoch)
		ring = consistenthash.NewConsistentHashRing()
		for _, node := range cm.Nodes {
			ring.Add(&node)
		}

		nodes, err := ring.HashToNodes(*Objectname, *Copies)
		if err != nil {
			fmt.Println(err)
			os.Exit(-1)
		}
		for i := range nodes {
			fmt.Print(nodes[i].NodeId, " ")
		}
		fmt.Println()
	} else {
		fmt.Println("no cluster map yet, may there is not cluster now")
	}
	if *Watch == true {
		mapch := c.Watch(context.TODO(), membership.ClusterMap)
		for {
			select {
			case <-time.Tick(5 * time.Second):
				fmt.Printf("at epoch %d:\r\n", epoch)
				nodes, err := ring.HashToNodes(*Objectname, *Copies)
				if err != nil {
					fmt.Println(err)
					os.Exit(-1)
				}
				for i := range nodes {
					fmt.Print(nodes[i].NodeId, " ")
				}
				fmt.Println()
			case mresp := <-mapch:
				for _, ev := range mresp.Events {
					var cm membership.ClusterMessage
					derr := util.Decode(membership.MESSAGE_CLUSTER, ev.Kv.Value, &cm)
					if derr != nil {
						log.Fatal("failed to decode cluster map")
					}
					epoch = cm.Ci.Epoch
					fmt.Printf("at epoch %d:\r\n", epoch)
					ring.RemoveAllNodes()
					for _, node := range cm.Nodes {
						ring.Add(&node)
					}
					nodes, err := ring.HashToNodes(*Objectname, *Copies)
					if err != nil {
						fmt.Println(err)
						os.Exit(-1)
					}
					for i := range nodes {
						fmt.Print(nodes[i].NodeId, " ")
					}
					fmt.Println()
				}
			}
		}
	}
}
